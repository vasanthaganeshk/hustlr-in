---
title: LSP, haskell-interactive-mode, landscape of tooling in the Haskell Universe for Emacs
---

The tooling landscape for text editors has gotten pretty interesting in these
years with the advent of 
[VS Code](https://en.wikipedia.org/wiki/Visual_Studio_Code). Yes it is the text
editor released by Microsoft and yes it runs on 
[electron](https://en.wikipedia.org/wiki/Electron_(software_framework)). What 
has it changed in the landscape of tooling for languages you might ask.

**Enter Language server protocol.**

> The Language Server Protocol (LSP) is an open, JSON-RPC-based protocol for use
> between source code editors or integrated development environments (IDEs) and 
> servers, providing programming language-specific features from the language 
> server to the client. The goal of the protocol is to allow programming language
> support to be decoupled from any given editor or IDE and even have it 
> distributed over independent language servers.

*https://en.wikipedia.org/wiki/Language_Server_Protocol*

Again, LSP is an open protocol that can be used by text-editors or IDEs to talk
to a process (LSP server) that will provide interactive editing features such as
auto-completion, goto function definition, refactorings, documenation etc.

Why should I care?

From the user's perspective nothing changes. But in terms of tooling for a 
language this is a big deal. If you want xyz-lang support for Emacs (the 
interactive editing part), and it doesn't have it, then you write an new 
minor-mode for it (think about 
[commint mode](https://www.emacswiki.org/emacs/ComintMode)). Same goes for Vim
and Atom and any other text editor or IDE of your choice. What if you had to 
write one plugin (or mode in Emacs's parlance) and it works everywhere... Sounds
good right? But still you need a client that speaks LSP on your text editor.

Also remember, using LSP does not make things magically faster or make you 
architecture superior. It is just a protocol.

Lets talk about Emacs.

Does LSP mean that you can forget about Elisp?

No, You still need Elisp for the LSP-client, configuration, syntax-highlighting,
indentation and more.

Again, LSP is for interactive editing features.

So what happens to existing modes such as [SLIME](https://en.wikipedia.org/wiki/SLIME), [haskell-mode](https://github.com/haskell/haskell-mode) etc?

It is upto the maintainers to bring support for LSP. For SLIME, there is 
already a Swank server that is used to provide interactive editing features. It
might make sense to reimplement it to support LSP  (because swank is written in
Common Lisp).

If you take a look at the 
[haskell-interactive-mode](https://github.com/vasanthaganeshk/haskell-interactive-mode),
it is completely written in Elisp. So lets say haskell-mode speaks LSP but then
in order to use it with other text editors you will need Emacs for that plugin 
to work and you have to install haskell-interactive-mode from MELPA, not many 
non Emacs users are going to want to use that plugin.

But think about the possibilites, if there were one plugin that works for all
text editors (for that language of course), you have a wider audience group 
and you could potentially get more bug reports, more contributions etc.

There is one more thing, LSP is useful only if a handfull of text editors decide
to implement it into themselves. By any means the adoption is going to be a 
little slow.

One interesting thing that I noticed was that RMS (Richard Stallman) supports
LSP and wants it to be a part of Emacs. 
[Here](https://lists.gnu.org/archive/html/emacs-devel/2017-04/msg00798.html)
is the email conversation.

**What do I have to do with all these?**

Surprise, Surprise I got selected for [Haskell Summer of Code](https://summer.haskell.org/)
and I am going to work on haskell-mode, more specifically I'm going to work on 
haskell-interactive-mode. I'm going to give haskell-interactive-mode a new life
of its own (Remove haskell-interactive-mode from haskell-mode and make 
haskell-interactive-mode a new repository). I'll also improve the tests suite
and implement some new features. [Gracjan Polak](https://github.com/gracjan)
is my mentor for this HSoC journey. The news has been published on the the HSoC
website for quite sometime now, 
click [here](https://summer.haskell.org/news/2017-05-24-accepted-projects.html)
to see that.

For me SLIME is the gold standard and I want to imitate it in all ways (for 
haskell-interactive-mode). I was exploring that for feature parity. My mentor
told me about the existence of LSP. Then I took a look at LSP. I thought LSP
was awesome. I found that [hie](https://github.com/haskell/haskell-ide-engine)
(haskell-ide-engine) is already implementing it. That is good; even more good 
news is that someone is working on improving LSP support for hie via HSoC.

**Tooling in the haskell universe for Emacs.**

So there is hie. Then I'm working on haskell-interactive-mode. Then there is Intero
(this tool provides interactive editing and complements haskell-mode, but you have
to use stack to use intero). There are other projects too. This landscape 
makes me happy because users have choice (This means Haskell is becoming popular
in my own metrics).
