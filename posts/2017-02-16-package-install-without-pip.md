---
title: Understanding to install packages in Python without PIP
---

PIP has made our lives so much more easier. It is a real pain if you want to install packages for python from source. Take `libseccomp` for example, it does not have a python package yet.

First things first, there are four locations in which your python modules may reside. They are as follows:

+ `/usr/lib/python2.7/dist-packages`
+ `/usr/lib/python2.7/site-packages`
+ `/usr/local/lib/python2.7/dist-packages`
+ `/usr/local/lib/python2.7/site-packages`

Any of these may or may not exist depending on you Linux distribution. If they do exist then they have some purpose. The ones with `/usr/lib/*` are reserved for the Python’s builtin modules. The ones that start with `/usr/local/*` are the ones available for our use. PIP installs under the `dist-packages` in `/usr/local` by default. If you compile from source and install it with make, it goes under `site-packages` in `/usr/local`. Now that we got that cleared, what is the problem?

Lets take Fedora 25(Linux distro), if you look at the `sys.path` of Python, there are some directories along with the first three in the above list. Now that you install with make, it goes to the fourth directory by default. After the package is installed and you cannot import the package. You have to include the snippet below before importing the package that you installed.

```
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
```

If you are using Fedora, just change the installation destination to `/usr/lib/*` then its installed properly and you can use it.

Now lets take a look at the scenario of Ubuntu 16.04. Only first and third are available. So no matter what destination you install your packages to, you cannot directly import your packages. You have to use the code snippet above. Otherwise you have to modify the `PYTHONPATH`  in your `.bashrc` and that is definitely not pretty. If you want an even uglier solution to avoid that code snippet, just copy the installed packages from `site-packages` to `dist-packages`. The last one might burn your computer, so sacrifice a cat before moving ahead.