---
title: libseccomp for Python2.7
---
‘libseccomp’ is a great high-level library for using the seccomp-bpf and related system calls. LWN has a great post to get started with python version of the library [here](https://lwn.net/Articles/634391/). For the C version of the library get started [here](https://blog.yadutaf.fr/2014/05/29/introduction-to-seccomp-bpf-linux-syscall-filter/). The installation of the C version is very obvious, use your package manager dnf on Fedora or apt on Ubuntu and find a package named libseccomp-devel or something similar. For the python version of the library, you have to install it from source. Download the tarball from [here](https://github.com/seccomp/libseccomp/releases/download/v2.3.1/libseccomp-2.3.1.tar.gz). Follow the steps given below to install the python version.

```	
sudo pip install cython
```
+ (this step is only for Fedora users)
	
```
sudo dnf install redhat-rpm-config
```

+ (this step is only for Fedora users)

```
sudo dnf install python-devel
```

+ (this step is only for Debian/Ubuntu users)

```
sudo apt install python-dev
```

+ unzip the tarball
+ cd into the libseccomp-2.x directory.

```
./configure --enable-python --prefix=/usr
make V=0	
sudo make install
```
If you are on Ubuntu 16.04 or greater. You are out of luck. In Ubuntu, Python does not search for site-packages for modules. So you have to add the site-packages to sys.path as explained [here](https://stackoverflow.com/questions/122327/how-do-i-find-the-location-of-my-python-site-packages-directory). You can also copy the seccomp installed files from site-packages to dist-packages. The second method avoids the code that you have to write to include to get it working, but it is a hack.

And that is it! Now try to launch python interpreter and type “import  seccomp”. If it works we are golden. I would like to thank [Paul Moore](http://www.paul-moore.com/) for helping me out on the libseccomp’s google group.