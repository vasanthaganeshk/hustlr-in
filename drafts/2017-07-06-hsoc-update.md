---
title: HSoC status update
---

And I'm still alive and kicking... I've started contributing code (and that is
what is expected of the interns). I've been advocating for people to use free
software because it means freedom and I believe that RMS is the saviour that
that god blessed upon us.

I've been showing my support by publically refusing to use proprietary software.
I don't use proprietary software not because it is bad or I have high morals,
it is because I want people around me to be puzzled and ask me why I don't use
Facebook or Windows. They call me "Ubuntu Ganesh" or "Ubuntu" (and I use Fedora
and have been using it for a long time now). I'm happy that
I've been a means to spread the word about what freedom smells like.

One of my friends asked me, what I would do with the code if I got it. I told him that
the proprietary software would be a virus to my computer and it would sabotage
my freedom by sharing my private information. I told that evil coporations like
Facebook does that already and still he says that I will be pretty useless to
them. I have to agree with him about me being useless to Facebook and so.

I've always felt bad that I have never contributed back to all the free software
that I've been telling about. I was just using it, enjoying it and living with
this cool community. I wanted to contribute code. After a long and tiring
journey I have finally been able to countribute something back to the community.

I have contributed before but they were very small contributions that it didn't
matter. Now I've contributed to `haskell-mode` project. I implemented the
`hide-show` feature, where you can collapse a block of code to get the overall
picture of what the code is about.

HSoC is one of the reasons why I'm contributing to `haskell-mode` project.
`haskell-mode` is a great tool for Haskell users whose weapon of choice is
Emacs. Did I tell you that my pull request got merged? It did get merged.
It is not like it is the ground breaking useful feature of all time but I'm
happy with what I've done.

I've written alot of projects before but the way that I'm doing now seems
the right way to do it. **I've been writing tests my children!!!** Tests.
Yes I feel like a pro at last.

The primary thing that I singed up for is to split `haskell-interactive-mode`
from `haskell-mode` and improve it. For now I've forked the repository, setup docs at
`readthedocs.io` (I learnt ReST on the way). Then I've setup Travis and
AppVeyor (I reused most of what was already available).

Then I started working on the REPL. There are two REPLs in haskell-mode.
The first one is a custom created on (haskell-interactive-mode). In this
everything has been implemented manually. The command history, ielm like
shortcuts and so on.

On the other hand, there is a depricated `inferior-haskell-mode`
that is based on the `comint-mode`. I've decided to improve on the
`comint-mode` not because it is easy to maintain or it is well documented
but because it is easy to use. It comes with many nifty functions.

So the repl (based on comint mode) works now. It finds if there is a
`.stack.yml` file in the repository, also looks for a `.cabal` file.
It finds which repl to use and opens the one properly.

The work is on going, I'm yet to write tests and improve support. This
is just an update letting you know that I'm working on improving
`haskell-interactive-mode`.
