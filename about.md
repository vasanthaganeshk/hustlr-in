---
title: About
---

My name is Vasantha Ganesh Kanniappan. I am a free software enthusiast.
I am doing my Bachelors in Computer Science Engineering from Amrita
School of Engineering, Coimbatore, Tamil Nadu, India.

I am a [Stallmanist](http://stallmanism.wikidot.com/). I spend most of
my time gawking at my monitor, i.e. browsing the internet for something
or the other (typical stallmanist). I love sketching although I haven’t sketched
in a while. I know some yoga. I use Emacs for text editing. I use Fedora with
Gnome because I haven't grown a long enough beard to start using Gentoo.
I live in one of the most peaceful places in India, I live near the
foot hills of the western ghats in Coimbatore, Tamil Nadu. This place is truly
sublime. You can have a breath taking view of the hills if you were here
for the monsoon.

This blog was generated with [Hakyll](https://jaspervdj.be/hakyll/), the
theme was stolen from [Jekyll](https://jekyllrb.com/)'s default
[minima theme](https://github.com/jekyll/minima) and here is the
[code](https://gitlab.com/vasanthaganeshk/hustlr-in) for my blog.

My Nick on FreeNode is `GreySunshine`.

Here's my [resume](https://gitlab.com/vasanthaganeshk/resume/raw/master/vasa_resume.pdf)(PDF)
and [here](https://gitlab.com/vasanthaganeshk/resume) is the git repository.