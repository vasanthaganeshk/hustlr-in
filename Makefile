all:
	stack build
	stack exec site build

.PHONY: watch
watch:
	stack exec site watch

.PHONY: clean
clean:
	stack exec site clean

.PHONY: deploy
deploy:
	cd deploy/;\
	ansible-playbook -i inventory main.yml --ask-become-pass
