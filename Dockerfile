FROM ubuntu:latest
LABEL maintainer="vasanthaganesh.k@tuta.io"

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update &&\
    apt-get install -y wget make &&\
    wget -qO- https://get.haskellstack.org/ | sh

RUN stack install hakyll-sass-0.2.3 --resolver lts-8.9

COPY . /hustlr-in
WORKDIR /hustlr-in

RUN make

FROM alpine:latest
WORKDIR /
COPY --from=0 /hustlr-in/entrypoint.sh /entrypoint.sh
COPY --from=0 /hustlr-in/_site/ /uuu
RUN chmod 777 entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
