#!/bin/sh
set -e
SOURCE_DIR=/uuu
TARGET_DIR=/www
if [ $(find $TARGET_DIR -maxdepth 0 -type d -empty) 2>/dev/null) ]; then
  cp -r --preserve-all $SOURCE_DIR/* $TARGET_DIR/
fi
# continue Docker container initialization, execute CMD
exec $@
