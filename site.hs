--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid
import           Hakyll.Web.Sass (sassCompiler)
import           Hakyll
import           Control.Applicative
import           Data.Yaml
import           Data.Maybe
import qualified Data.ByteString.Char8 as BS

--------------------------------------------------------------------------------


data BlogConfig = BlogConfig {
  blogname :: String,
  tagline  :: String,
  author   :: String,
  email    :: String,
  twitter  :: String,
  gitlab   :: String,
  github   :: String
} deriving (Show)


instance FromJSON BlogConfig where
    parseJSON (Object v) = BlogConfig <$>
                           v .: "blogname" <*>
                           v .: "tagline" <*>
                           v .: "author" <*>
                           v .: "email" <*>
                           v .: "twitter" <*>
                           v .: "gitlab" <*>
                           v .: "github"

    -- A non-Object value is of the wrong type, so fail.
    parseJSON _ = error "Can't parse BlogConfig from YAML"

decodedstr :: BS.ByteString -> BlogConfig
decodedstr = fromJust . decode

main = do
  blg   <- BS.readFile "blog-config.yaml"

  let blogconfig = decodedstr blg
  hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*.scss" $ do
      route $ setExtension "css"
      let compressCssItem = fmap compressCss
      compile (compressCssItem <$> sassCompiler)

    match (fromList ["about.md", "contact.md"]) $ do
        route   $ setExtension "html"

        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" (defaultCTX blogconfig)
            >>= relativizeUrls


    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    (postCtx blogconfig)
            >>= loadAndApplyTemplate "templates/default.html" (postCtx blogconfig)
            >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" (archiveCtx posts blogconfig)
                >>= loadAndApplyTemplate "templates/default.html" (archiveCtx posts blogconfig)
                >>= relativizeUrls

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"

            getResourceBody
                >>= applyAsTemplate (indexCtx posts blogconfig)
                >>= loadAndApplyTemplate "templates/default.html" (indexCtx posts blogconfig)
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------

defaultCTX blogconfig = constField "blogname" (blogname blogconfig) <>
                        constField "author" (author blogconfig)     <>
                        constField "email" (email blogconfig)       <>
                        constField "tagline" (tagline blogconfig)   <>
                        constField "twitter" (twitter blogconfig)   <>
                        constField "gitlab" (gitlab blogconfig)     <>
                        constField "github" (github blogconfig)     <>
                        defaultContext

postCtx blogconfig = dateField "date" "%B %e, %Y" <>
                     defaultCTX blogconfig

archiveCtx posts  blogconfig = listField "posts" (postCtx blogconfig) (return posts) <>
                               constField "title" "Archive"                          <>
                               defaultCTX blogconfig


indexCtx posts blogconfig =  listField "posts" (postCtx blogconfig) (return (take 5 posts)) <>
                             constField "title" "Posts"                                     <>
                             defaultCTX blogconfig
